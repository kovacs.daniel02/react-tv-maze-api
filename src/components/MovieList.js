
export function MovieList({ elemek }) {
    return (
        <div className="row justify-content-center">
            {elemek.map((elem) => (
                <div key={elem.show.id} className="card mx-3 my-3 text-center" style={{ width: 200, height: 420 }}>
                    <img className="card-img-top"
                        alt="Not found"
                        src=
                        {
                            elem.show.image !== null ?
                                elem.show.image.medium :
                                "https://via.placeholder.com/210x295"
                        } />
                    <div className="card-body">
                        <h5 className="card-title">{elem.show.name}</h5>
                        <p className="card-text">
                            {
                                elem.show.rating.average !== null ?
                                    elem.show.rating.average :
                                    "Nincs értékelése"
                            }
                        </p>
                    </div>
                </div>
            ))}
        </div>
    );
}