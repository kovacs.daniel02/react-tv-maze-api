
export function NavBar({ setName }) {
    return (
        <nav className="navbar navbar-light bg-light">
            <p className="navbar-brand">TV Maze API kereső</p>
            <form
                className="form-inline"
                onSubmit={(event) => {
                    event.preventDefault();
                    setName(event.target.elements.moviename.value);
                }}
            >
                <input className="form-control mr-sm-2" type="search" placeholder="Search" name="moviename" />
                <button className="btn btn-primary" type="submit">Search</button>
            </form>
        </nav>
    );
}