import './App.css';
import { useState, useEffect } from 'react';
import { NavBar} from './components/NavBar';
import { MovieList } from './components/MovieList';

function App() {
    const [movies, setMovies] = useState([]);
    const [name, setName] = useState("");

    useEffect(() => {
        fetch("http://api.tvmaze.com/search/shows?q=" + name)
            .then((res) => (res.ok ? res.json() : []))
            .then((tartalom) => {
                setMovies(tartalom)
            })
    }, [name])

    return (
        <div className="container">
            <NavBar setName={setName} />
            <MovieList elemek={movies} />
        </div>
    )
}

export default App;
